# Event sourcing pattern

## modelo de event sourcing usando as libs @irontitan/paradox, @expresso/expresso

### pré requisito

Antes de começar você precisa de uma database mongodb, sugiro um container docker

```bash
# Com o docker instalado rode:
$ docker run --name mongodb-event-sourcing -p 27017:27017 -d mongo
```

## rodando o projeto

```bash
# Instale as dependências
$ npm install

# Carregue as variáveis de ambiente
$ source .envrc

# Iniciar projeto
$ npm start

# Para modo de desenvolvimento
$ npm run dev
```

<https://www.getpostman.com/collections/64b18a37b617a2e92813>
importe esse link como uma collection no postman.
