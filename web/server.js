"use strict";

const app = require("./app");
const config = require("../config");
const { server } = require("@expresso/expresso");

server.start(app, config);
