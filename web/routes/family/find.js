"use strict";

const rescue = require("express-rescue");

const factory = (service) => [
  rescue(async (req, res) => {
    const { state: family } = await service.find(req.params.person);

    res.status(200).json(family);
  }),
  (err, req, res, next) => {
    // Person.custom_errors

    next(err);
  },
];

module.exports = { factory };
