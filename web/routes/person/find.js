"use strict";

const rescue = require("express-rescue");

const factory = (service) => [
  rescue(async (req, res) => {
    const { state: person } = await service.find(req.params.person);

    res.status(200).json(person);
  }),
  (err, req, res, next) => {
    // Person.custom_errors

    next(err);
  },
];

module.exports = { factory };
