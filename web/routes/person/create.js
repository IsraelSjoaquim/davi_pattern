"use strict";

const rescue = require("express-rescue");
const { validate } = require("@expresso/expresso");

const factory = (service) => [
  validate({
    type: "object",
    required: ["name", "email", "familyId"],
    properties: {
      name: {
        type: "string",
      },
      familyId: {
        type: "string",
      },
      email: {
        type: "string",
      },
    },
  }),
  rescue(async (req, res) => {
    const { id } = await service.create(req.body, "Tryp");

    res.status(201).json({ id });
  }),
  (err, req, res, next) => {
    // Person.custom_errors

    next(err);
  },
];

module.exports = { factory };
