'use strict'

const rescue = require('express-rescue')

const factory = (service) => ([
  rescue(async (req, res) => {
    await service.delete(req.params.person, "Tryp")

    res.status(204)
      .end()
  })
])

module.exports = { factory }
