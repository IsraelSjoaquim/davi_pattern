"use strict";

const rescue = require("express-rescue");
const { validate, HttpError } = require("@expresso/expresso");

const factory = (service) => [
  validate({
    type: "object",
    require: ["name", "email"],
    properties: {
      name: {
        type: "string",
      },
      email: {
        type: "string",
      },
    },
  }),
  rescue(async (req, res) => {
    await service.update(req.params.person, req.body, "Tryp");
    
    res.status(204).end();
  }),
  (err, req, res, next) => {
    // Person.custom_errors
    next(err);
  },
];

module.exports = { factory };
