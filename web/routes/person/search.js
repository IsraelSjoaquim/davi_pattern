"use strict";

const { format } = require("util");
const rescue = require("express-rescue");
const { validate } = require("@expresso/expresso");

const SortDirection = {
  asc: 1,
  des: -1,
};

const factory = (service) => [
  validate.query({
    type: "object",
    properties: {
      q: {
        type: "string",
      },
      statuses: {
        type: "array",
        items: {
          type: "string",
        },
      },
      page: {
        type: "string",
        minimum: 1,
      },
      size: {
        type: "string",
        minimum: 1,
        maximum: 200,
      },
      sortBy: {
        type: "string",
      },
      sortDirection: {
        type: "string",
        enum: ["asc", "des"],
      },
      additionalProperties: false,
    },
  }),
  rescue(async (req, res) => {
    const {
      page,
      size,
      q: term,
      sortBy,
      sortDirection,
      ...filters
    } = req.query;

    const { person, count, range, total } = await service.search(
      filters,
      term,
      { page, size, sortBy, sortDirection: SortDirection[sortDirection] }
    );

    const status = count >= total ? 200 : 206;

    if (status === 206) {
      res.append(
        "x-content-range",
        format("results %s-%s/%s", range.from, range.to, total)
      );
    }

    res.status(status).json(person.map((person) => person.state));
  }),
];

module.exports = { factory };
