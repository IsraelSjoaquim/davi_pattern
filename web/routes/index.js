"use strict";

module.exports = {
  person: {
    find: require("./person/find"),
    create: require("./person/create"),
    search: require("./person/search"),
    update: require("./person/update"),
    delete: require("./person/delete"),
  },
  family: {
    find: require("./family/find"),
    create: require("./family/create"),
    search: require("./family/search")
  }
};
