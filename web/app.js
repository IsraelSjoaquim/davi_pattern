"use strict";

const expresso = require("@expresso/expresso");
const routes = require("./routes");
const partialResponse = require("express-partial-response");

const database = require("../infra/database");

/**
 * Repositories
 * ============
 */
const PersonRepository = require("../infra/repositories/person");
const FamilyRepository = require("../infra/repositories/family")

/**
 * Services
 * ========
 */
const PersonService = require("../services/Person");
const FamilyService = require("../services/Family")

module.exports = expresso(async (app, config) => {
  const { mongodbConnection } = await database.factory(config.database);

  const personRepository = new PersonRepository(mongodbConnection);
  const familyRepository = new FamilyRepository(mongodbConnection)

  const familyService = new FamilyService(familyRepository)
  const personService = new PersonService(personRepository, familyService);

  app.use(partialResponse());

  app.get("/person", routes.person.search.factory(personService));
  app.post("/person", routes.person.create.factory(personService));
  app.get("/person/:person", routes.person.find.factory(personService));
  app.put("/person/:person", routes.person.update.factory(personService));
  app.delete("/person/:person", routes.person.delete.factory(personService));

  app.get("/family", routes.family.search.factory(familyService))
  app.post("/family", routes.family.create.factory(familyService))
  app.get("/family/:family", routes.family.find.factory(familyService))


});
