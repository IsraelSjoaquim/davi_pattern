"use strict";

const { ObjectId } = require("mongodb");
const { MongodbEventRepository } = require("@irontitan/paradox");

const Family = require("../../domain/family/Family");

const queryFactory = (filters, term) => {
  let { statuses = [] } = filters;
  const regex = new RegExp(term);

  const conditions = [
    {
      $or: [{ "state.name": regex }],
    },
  ];

  if (!Array.isArray(statuses)) {
    statuses = [statuses];
  }  

  return { $and: conditions };
};

class FamilyRepository extends MongodbEventRepository {
  constructor(connection) {
    super(connection.collection(Family.collection), Family);
  }

  async findById(id) {
    const family = await this._collection
      .find({ _id: ObjectId(id) })
      .limit(1)
      .toArray()
      .then(([result]) => result);

    if (!family) {
      return null;
    }

    return new Family().setPersistedEvents(family.events);
  }

  async search(
    filters = {},
    term = "",
    page = 1,
    size = 50,
    sortBy = "name",
    sortDirection = 1
  ) {
    const query = queryFactory(filters, term);
    const sort = { [`state.${sortBy}`]: sortDirection };

    const { documents, count, range, total } = await this._runPaginatedQuery(
      query,
      page,
      size,
      sort
    );

    const family = documents.map(({ events }) => {
      return new Family().setPersistedEvents(events);
    });

    return { family, count, range, total };
  }
}

module.exports = FamilyRepository;
