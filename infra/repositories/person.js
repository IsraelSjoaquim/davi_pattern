"use strict";

const { ObjectId } = require("mongodb");
const { MongodbEventRepository } = require("@irontitan/paradox");

const Person = require("../../domain/person/Person");

const queryFactory = (filters, term) => {
  let { statuses = [] } = filters;
  const regex = new RegExp(term);

  const conditions = [
    {
      $or: [{ "state.name": regex }, { "state.email": regex }],
    },
  ];

  if (!Array.isArray(statuses)) {
    statuses = [statuses];
  }

  if (statuses.length === 1) {
    conditions.push({
      "state.deletedAt": {
        [statuses[0] === "active" ? "$eq" : "$ne"]: null,
      },
    });
  }

  return { $and: conditions };
};

class PersonRepository extends MongodbEventRepository {
  constructor(connection) {
    super(connection.collection(Person.collection), Person);
  }

  async findById(id) {
    const person = await this._collection
      .find({ _id: ObjectId(id) })
      .limit(1)
      .toArray()
      .then(([result]) => result);

    if (!person) {
      return null;
    }

    return new Person().setPersistedEvents(person.events);
  }

  async search(
    filters = {},
    term = "",
    page = 1,
    size = 50,
    sortBy = "name",
    sortDirection = 1
  ) {
    const query = queryFactory(filters, term);
    const sort = { [`state.${sortBy}`]: sortDirection };

    const { documents, count, range, total } = await this._runPaginatedQuery(
      query,
      page,
      size,
      sort
    );

    const person = documents.map(({ events }) => {
      return new Person().setPersistedEvents(events);
    });

    return { person, count, range, total };
  }
}

module.exports = PersonRepository;
