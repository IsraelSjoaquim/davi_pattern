"use strict";

const { MongoClient } = require("mongodb");

/**
 * @type {Object} MongoClient default settings.
 */
const defaults = {
  poolSize: 10,
  useNewUrlParser: true,
  useUnifiedTopology: true,
};

/**
 * @param  {String}                               arg.url     MongoDB connection URL.
 * @param  {String}                               arg.dbname  DB name.
 * @param  {Object}                               arg.options MongoClient options.
 * @returns {Promise.<Object.<String, Function>>}             Methods.
 */
const connect = async ({ url, dbname, options = {} }) => {
  const client = await MongoClient.connect(url, { ...defaults, ...options });

  return client.db(dbname);
};

module.exports = { connect };
