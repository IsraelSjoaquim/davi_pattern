"use strict";

const env = require("sugar-env");

module.exports = {
  database: {
    mongodb: {
      url: env.get("MONGODB_URL"),
      dbname: env.get("MONGODB_DBNAME"),
    },
  },
};
