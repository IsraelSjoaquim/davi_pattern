"use strict";

const Person = require("../domain/person/Person");

class PersonService {
  constructor(repository, familyService) {
    this._repository = repository;
    this._familyService = familyService
  }

  async create(params, user) {
    const family = await this._familyService.find(params.familyId)
    delete params.familyId

    const createdPerson = Person.create({...params, family}, user);    

    return this._repository.save(createdPerson);
  } 

  async update(id, params, user) {   
    const person = await this.find(id);
    person.update(params, user);
    await this._repository.save(person);
  }

  async delete (id, user) {
    const person = await this._repository.findById(id)

    if (!person) {
      return
    }

    if (person.deletedAt) {
      return
    }

    person.delete(user)

    await this._repository
      .save(person)
  }

  async find(id) {
    const person = await this._repository.findById(id);

    if (!person) {
      throw new Error();
    }

    return person;
  }

  async search(filters, term, pagination) {
    const {
      page = 1,
      size = 50,
      sortBy = "name",
      sortDirection = 1,
    } = pagination;

    return this._repository.search(
      filters,
      term,
      parseInt(page),
      parseInt(size),
      sortBy,
      sortDirection
    );
  }
}

module.exports = PersonService;
