"use strict";

const Family = require("../domain/family/Family");

class FamilyService {
  constructor(repository) {
    this._repository = repository;
  }

  async create(params, user) {
    const createdFamily = Family.create(params, user);
    return this._repository.save(createdFamily);
  }  

  async find(id) {
    const family = await this._repository.findById(id);

    if (!family) {
      throw new Error();
    }

    return family;
  }

  async search(filters, term, pagination) {
    const {
      page = 1,
      size = 50,
      sortBy = "name",
      sortDirection = 1,
    } = pagination;

    return this._repository.search(
      filters,
      term,
      parseInt(page),
      parseInt(size),
      sortBy,
      sortDirection
    );
  }
}

module.exports = FamilyService;
