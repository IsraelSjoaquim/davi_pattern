"use strict";

const { EventEntity } = require("@irontitan/paradox");
const { ObjectId } = require("mongodb");

const FamilyWasCreated = require("./events/FamilyWasCreatedEvent");

class Family extends EventEntity {
  constructor() {
    super({
      [FamilyWasCreated.eventName]: FamilyWasCreated.commit,
    });
    this.id = null;
    this.name = null;
    this.createdAt = null;
    this.createdBy = null;
  }

  static get collection() {
    return "family";
  }  

  get state() {
    const currentState = this.reducer.reduce(new Family(), [
      ...this.persistedEvents,
      ...this.pendingEvents,
    ]);

    return {
      id: currentState.id,
      name: currentState.name,      
      createdAt: currentState.createdAt,
      createdBy: currentState.createdBy,      
    };
  }

  static create(data, user) {
    const family = new Family();

    family.pushNewEvents([
      new FamilyWasCreated({ id: new ObjectId(), ...data}, user),
    ]);

    return family;
  }
  
}

module.exports = Family;
