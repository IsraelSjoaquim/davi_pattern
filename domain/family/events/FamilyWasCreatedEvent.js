"use strict";

const { Event } = require("@irontitan/paradox");

const merge = require("lodash.merge");

class FamilyWasCreated extends Event {
  static get eventName() {
    return "Family-was-created";
  }

  constructor(data, user) {
    super(FamilyWasCreated.eventName, data);
    this.user = user;
  }

  static commit(state, { data, timestamp, user }) {
    const { id, name } = data;

    return merge(
      { ...state },
      {
        id,
        name,
        createdAt: timestamp,
        createdBy: user,
      }
    );
  }
}

module.exports = FamilyWasCreated;
