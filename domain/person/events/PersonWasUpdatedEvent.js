"use strict";
const { Event } = require("@irontitan/paradox");
const merge = require("lodash.merge");

class PersonWasUpdated extends Event {
  static get eventName() {
    return "person-was-changed";
  }

  constructor(data, user) {
    super(PersonWasUpdated.eventName, data);
    this.user = user;
  }

  static commit(state, { data, timestamp, user }) {
    const { name, email } = data;

    return merge(
      { ...state },
      {
        name,
        email,
        updatedAt: timestamp,
        updatedBy: user,
      }
    );
  }
}

module.exports = PersonWasUpdated;
