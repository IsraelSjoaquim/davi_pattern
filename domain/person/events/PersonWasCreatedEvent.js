"use strict";

const { Event } = require("@irontitan/paradox");

const merge = require("lodash.merge");

class PersonWasCreated extends Event {
  static get eventName() {
    return "person-was-created";
  }

  constructor(data, user) {
    super(PersonWasCreated.eventName, data);
    this.user = user;
  }

  static commit(state, { data, timestamp, user }) {
    const {
      id,      
      family: { id: familyId },
      name,
      email,
    } = data;

    return merge(
      { ...state },
      {
        id,
        familyId,
        name,
        email,
        createdAt: timestamp,
        createdBy: user,
      }
    );
  }
}

module.exports = PersonWasCreated;
