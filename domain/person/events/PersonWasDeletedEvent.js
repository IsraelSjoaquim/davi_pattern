'use strict'
const merge = require('lodash.merge')
const { Event } = require('@irontitan/paradox')

class PersonWasDeleted extends Event {
  constructor(user) {
    super(PersonWasDeleted.eventName, {})
    this.user = user
  }

  static get eventName () { return 'person-was-deleted' }

  static commit (state, { timestamp, user }) {
    return merge({ ...state }, {
      deletedAt: timestamp,
      deletedBy: user
    })
  }
}

module.exports = PersonWasDeleted
