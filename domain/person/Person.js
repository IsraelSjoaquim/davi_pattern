"use strict";

const { EventEntity } = require("@irontitan/paradox");
const { ObjectId } = require("mongodb");

const PersonWasCreated = require("./events/PersonWasCreatedEvent");
const PersonWasUpdated = require("./events/PersonWasUpdatedEvent");
const PersonWasDeleted = require("./events/PersonWasDeletedEvent");

const removeEvents = (data) => {
  Object.keys(data).forEach((key)=>{
    if (data[key] && typeof data[key] == "object" ){      
      if(data[key].persistedEvents) delete data[key].persistedEvents
      if(data[key].pendingEvents) delete data[key].pendingEvents
      if(data[key].reducer) delete data[key].reducer
    } 
  })  
  return data
}

class Person extends EventEntity {
  constructor() {
    super({
      [PersonWasCreated.eventName]: PersonWasCreated.commit,
      [PersonWasUpdated.eventName]: PersonWasUpdated.commit,
      [PersonWasDeleted.eventName]: PersonWasDeleted.commit,
    });
    this.id = null;
    this.familyId = null;
    this.name = null;
    this.email = null;
    this.createdAt = null;
    this.createdBy = null;
    this.updatedAt = null;
    this.updatedBy = null;
    this.deletedAt = null;
    this.deletedBy = null;
  }

  static get collection() {
    return "people";
  }  

  get state() {
    const currentState = this.reducer.reduce(new Person(), [
      ...this.persistedEvents,
      ...this.pendingEvents,
    ]);

    return {
      id: currentState.id,
      familyId: currentState.familyId,
      name: currentState.name,
      email: currentState.email,
      createdAt: currentState.createdAt,
      createdBy: currentState.createdBy,
      updatedAt: currentState.updatedAt,
      updatedBy: currentState.updatedBy,
      deletedAt: currentState.deletedAt,
      deletedBy: currentState.deletedBy,
    };
  }

  static create(data, user) {
    const person = new Person();
    removeEvents(data)

    person.pushNewEvents([
      new PersonWasCreated({ id: new ObjectId(), ...data}, user),
    ]);

    return person;
  }

  update(data, user) {
    removeEvents(data)

    this.pushNewEvents([new PersonWasUpdated(data, user)]);

    return this;
  }

  delete(user) {
    this.pushNewEvents([new PersonWasDeleted(user)]);

    return this;
  }
}

module.exports = Person;
